package org.fullguys.swingsynchronizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class SwingSynchronizerApplication {

    public static void main(String[] args) {
        System.exit(
                SpringApplication.exit(SpringApplication.run(SwingSynchronizerApplication.class, args)));
    }
}
