package org.fullguys.swingsynchronizer.adapters.file;

import joinery.DataFrame;
import org.fullguys.swingsynchronizer.adapters.file.utils.Constants;
import org.fullguys.swingsynchronizer.common.annotations.FileAdapter;

import java.io.IOException;
import java.io.InputStream;

@FileAdapter
public class ReaderAdapter {

    public DataFrame<Object> readFile(InputStream datafile) throws IOException {
        return DataFrame.readCsv(datafile, Constants.SEMICOLON);
    }
}
