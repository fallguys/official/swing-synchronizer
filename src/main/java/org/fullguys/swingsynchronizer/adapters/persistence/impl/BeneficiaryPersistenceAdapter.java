package org.fullguys.swingsynchronizer.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.adapters.persistence.mapper.BeneficiaryMapper;
import org.fullguys.swingsynchronizer.common.annotations.PersistenceAdapter;
import org.fullguys.swingsynchronizer.components.database.models.BeneficiaryModel;
import org.fullguys.swingsynchronizer.components.database.repository.BeneficiaryRepository;
import org.fullguys.swingsynchronizer.domain.entities.Beneficiary;
import org.fullguys.swingsynchronizer.domain.entities.enums.BeneficiaryState;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.out.GetBeneficiariesIDByCodesPort;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.out.SaveBeneficiariesPort;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.out.UpdateBeneficiaryStatePort;
import org.javatuples.Pair;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Transactional
@PersistenceAdapter
@RequiredArgsConstructor
public class BeneficiaryPersistenceAdapter implements GetBeneficiariesIDByCodesPort, UpdateBeneficiaryStatePort, SaveBeneficiariesPort {

    private final BeneficiaryRepository beneficiaryRepository;
    private final BeneficiaryMapper beneficiaryMapper;

    @Override
    public Map<String, Pair<Long, LocalDateTime>> getBeneficiariesIDByCodes(List<String> beneficiariesCodes) {
        return beneficiaryRepository
                .findAllByCodeIn(beneficiariesCodes)
                .stream()
                .collect(Collectors.toMap(
                        BeneficiaryModel.CodeAndIdModel::getCode, beneficiary ->
                                Pair.with(beneficiary.getId(), beneficiary.getCreatedAt())));
    }

    @Override
    public void updateBeneficiaryState(BeneficiaryState state) {
        beneficiaryRepository.setAllBeneficiariesState(state.name());
    }

    @Override
    public void saveBeneficiaries(List<Beneficiary> beneficiaries) {
        var rows = beneficiaryMapper.toBeneficiariesModels(beneficiaries);
        beneficiaryRepository.saveAll(rows);
    }
}
