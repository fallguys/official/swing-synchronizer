package org.fullguys.swingsynchronizer.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.adapters.persistence.mapper.ContagionAreasMapper;
import org.fullguys.swingsynchronizer.common.annotations.PersistenceAdapter;
import org.fullguys.swingsynchronizer.components.database.repository.ContagionAreaRepository;
import org.fullguys.swingsynchronizer.domain.entities.ContagionArea;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.out.GetContagionAreasPort;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.out.UpdateContagionAreasPort;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class ContagionAreasPersistenceAdapter implements GetContagionAreasPort, UpdateContagionAreasPort {

    private final ContagionAreaRepository contagionAreaRepository;
    private final ContagionAreasMapper contagionAreasMapper;

    @Override
    public List<ContagionArea> getContagionAreas() {
        var rows = contagionAreaRepository.findAll();
        return contagionAreasMapper.toContagionAreas(rows);
    }

    @Override
    public void updateContagionAreas(List<ContagionArea> contagionAreas) {
        var rows = contagionAreasMapper.toContagionAreaModels(contagionAreas);
        contagionAreaRepository.saveAll(rows);
    }
}
