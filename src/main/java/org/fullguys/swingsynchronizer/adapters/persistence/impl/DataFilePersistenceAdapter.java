package org.fullguys.swingsynchronizer.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.PersistenceAdapter;
import org.fullguys.swingsynchronizer.components.s3.client.Client;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out.GetDataFileToProcessPort;

import java.io.InputStream;

@PersistenceAdapter
@RequiredArgsConstructor
public class DataFilePersistenceAdapter implements GetDataFileToProcessPort {

    private final Client client;

    @Override
    public InputStream getDataFileProcess(String url){
        return client.downloadFile(url);
    }
}
