package org.fullguys.swingsynchronizer.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.adapters.persistence.mapper.WithdrawalPointMapper;
import org.fullguys.swingsynchronizer.common.annotations.PersistenceAdapter;
import org.fullguys.swingsynchronizer.components.database.models.WithdrawalPointModel;
import org.fullguys.swingsynchronizer.components.database.repository.WithdrawalPointRepository;
import org.fullguys.swingsynchronizer.domain.entities.WithdrawalPoint;
import org.fullguys.swingsynchronizer.domain.entities.enums.WithdrawalPointState;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in.SavePointsPort;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.out.GetPointsIDByCodesPort;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.out.UpdatePointsStatePort;
import org.javatuples.Pair;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Transactional
@PersistenceAdapter
@RequiredArgsConstructor
public class PointPersistenceAdapter implements GetPointsIDByCodesPort, UpdatePointsStatePort, SavePointsPort {

    private final WithdrawalPointRepository withdrawalPointRepository;
    private final WithdrawalPointMapper withdrawalPointMapper;

    @Override
    public Map<String, Pair<Long, LocalDateTime>> getPointsIDByCodes(List<String> pointsCodes) {
        return withdrawalPointRepository
                .findAllByCodeIn(pointsCodes)
                .stream()
                .collect(Collectors.toMap(
                        WithdrawalPointModel.CodeAndIDModel::getCode,
                        point -> Pair.with(point.getId(), point.getCreatedAt())));
    }

    @Override
    public void updatePointsState(WithdrawalPointState pointsState) {
        withdrawalPointRepository.setAllPointsState(pointsState.name());
    }

    @Override
    public void savePoints(List<WithdrawalPoint> points) {
        var rows = withdrawalPointMapper.toWithdrawalPointModels(points);
        withdrawalPointRepository.saveAll(rows);
    }
}
