package org.fullguys.swingsynchronizer.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.adapters.persistence.mapper.SynchroMapper;
import org.fullguys.swingsynchronizer.common.annotations.PersistenceAdapter;
import org.fullguys.swingsynchronizer.components.database.repository.SynchroRepository;
import org.fullguys.swingsynchronizer.components.database.repository.SynchroResultRepository;
import org.fullguys.swingsynchronizer.domain.entities.Synchro;
import org.fullguys.swingsynchronizer.domain.entities.enums.SynchroState;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out.GetSynchroByStatePort;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out.UpdateSynchroPort;

import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class SynchroPersistenceAdapter implements GetSynchroByStatePort, UpdateSynchroPort {

    private final SynchroResultRepository synchroResultRepository;
    private final SynchroRepository synchroRepository;
    private final SynchroMapper synchroMapper;

    @Override
    public Optional<Synchro> getSynchroByState(SynchroState synchroState) {
        var synchroRow = synchroResultRepository.findBySynchroState(synchroState.name());
        return synchroRow.map(synchroMapper::toSynchro);
    }

    @Override
    public void updateSynchro(Synchro synchro) {
        var synchroResultRow = synchroMapper.toSynchroResultModel(synchro);
        var synchroRow = synchroRepository.save(synchroResultRow.getSynchro());
        synchroResultRow.setSynchro(synchroRow);
        synchroResultRepository.save(synchroResultRow);
    }
}
