package org.fullguys.swingsynchronizer.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.adapters.persistence.mapper.UbigeeMapper;
import org.fullguys.swingsynchronizer.common.annotations.PersistenceAdapter;
import org.fullguys.swingsynchronizer.components.database.repository.UbigeeRepository;
import org.fullguys.swingsynchronizer.domain.entities.Ubigee;
import org.fullguys.swingsynchronizer.domain.entities.enums.UbigeeType;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out.GetUbigeeByTypePort;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class UbigeePersistenceAdapter implements GetUbigeeByTypePort {

    private final UbigeeRepository ubigeeRepository;
    private final UbigeeMapper ubigeeMapper;

    @Override
    public List<Ubigee> getUbigeeByType(UbigeeType ubigeeType) {
        var rows = ubigeeRepository.findByType(ubigeeType.toString());
        return ubigeeMapper.toUbigeeList(rows);
    }
}
