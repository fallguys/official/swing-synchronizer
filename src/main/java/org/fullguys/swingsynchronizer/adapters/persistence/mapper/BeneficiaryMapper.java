package org.fullguys.swingsynchronizer.adapters.persistence.mapper;

import org.fullguys.swingsynchronizer.components.database.models.BeneficiaryModel;
import org.fullguys.swingsynchronizer.domain.entities.Beneficiary;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface BeneficiaryMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "code", source = "code")
    @Mapping(target = "districtId", source = "ubigeeID")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "gender", source = "gender")
    @Mapping(target = "isDisabled", source = "isDisabled")
    @Mapping(target = "isOlderAdult", source = "isOlderAdult")
    @Mapping(target = "updatedAt", source = "updatedAt")
    @Mapping(target = "state", source = "state")
    BeneficiaryModel toBeneficiaryModel(Beneficiary beneficiary);

    List<BeneficiaryModel> toBeneficiariesModels(List<Beneficiary> beneficiary);
}
