package org.fullguys.swingsynchronizer.adapters.persistence.mapper;

import org.fullguys.swingsynchronizer.components.database.models.ContagionAreaModel;
import org.fullguys.swingsynchronizer.domain.entities.ContagionArea;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface ContagionAreasMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "districtId", target = "districtID")
    @Mapping(source = "quantity", target = "quantity")
    @Mapping(source = "createdAt", target = "createdAt")
    @Mapping(source = "updatedAt", target = "updatedAt")
    @Mapping(source = "population", target = "population")
    ContagionArea toContagionArea(ContagionAreaModel contagionAreaModel);

    List<ContagionArea> toContagionAreas(List<ContagionAreaModel> contagionAreaModels);

    @InheritInverseConfiguration
    ContagionAreaModel toContagionAreaModel(ContagionArea contagionArea);

    List<ContagionAreaModel> toContagionAreaModels(List<ContagionArea> contagionAreas);
}
