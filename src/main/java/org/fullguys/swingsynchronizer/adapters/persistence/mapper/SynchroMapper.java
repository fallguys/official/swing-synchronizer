package org.fullguys.swingsynchronizer.adapters.persistence.mapper;

import org.fullguys.swingsynchronizer.components.database.models.SynchroResultModel;
import org.fullguys.swingsynchronizer.domain.entities.Synchro;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface SynchroMapper {

    @Mapping(target = "id", source = "synchro.id")
    @Mapping(target = "administratorId", source = "synchro.administratorId")
    @Mapping(target = "state", source = "synchro.state")
    @Mapping(target = "fileType", source = "synchro.fileType")
    @Mapping(target = "initialDate", source = "synchro.initialDate")
    @Mapping(target = "resultId", source = "id")
    @Mapping(target = "dataFileURL", source = "sourceDataFile")
    @Mapping(target = "errorFileURL", source = "errorDataFile")
    @Mapping(target = "registriesTotal", source = "totalRegistries")
    @Mapping(target = "failedRegistriesTotal", source = "failedRegistries")
    Synchro toSynchro(SynchroResultModel synchroResultModel);

    @InheritInverseConfiguration
    @Mapping(target = "synchroId", source = "id")
    @Mapping(source = "initialDate", target = "synchro.initialDate")
    @Mapping(source = "administratorId", target = "synchro.administratorId")
    @Mapping(target = "successfulRegistries", ignore = true)
    SynchroResultModel toSynchroResultModel(Synchro synchro);
}
