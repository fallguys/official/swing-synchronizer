package org.fullguys.swingsynchronizer.adapters.persistence.mapper;

import org.fullguys.swingsynchronizer.components.database.models.UbigeeModel;
import org.fullguys.swingsynchronizer.domain.entities.Ubigee;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface UbigeeMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "type", target = "type")
    })
    Ubigee toUbigee(UbigeeModel ubigeeModel);
    List<Ubigee> toUbigeeList(List<UbigeeModel> ubigeeModelList);

    @InheritInverseConfiguration
    @Mapping(target = "fatherUbigeeId", ignore = true)
    UbigeeModel toUbigeeModel(Ubigee ubigee);
}
