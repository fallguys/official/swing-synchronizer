package org.fullguys.swingsynchronizer.adapters.persistence.mapper;

import org.fullguys.swingsynchronizer.components.database.models.WithdrawalPointModel;
import org.fullguys.swingsynchronizer.domain.entities.WithdrawalPoint;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface WithdrawalPointMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "code", source = "code")
    @Mapping(target = "state", source = "state")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "districtId", source = "districtID")
    @Mapping(target = "weekdayHourStart", source = "weekdayHourStart")
    @Mapping(target = "weekdayHourEnd", source = "weekdayHourEnd")
    @Mapping(target = "weekendHourStart", source = "weekendHourStart")
    @Mapping(target = "weekendHourEnd", source = "weekendHourEnd")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "updatedAt", source = "updatedAt")
    WithdrawalPointModel toWithdrawalPointModel(WithdrawalPoint point);

    List<WithdrawalPointModel> toWithdrawalPointModels(List<WithdrawalPoint> points);
}
