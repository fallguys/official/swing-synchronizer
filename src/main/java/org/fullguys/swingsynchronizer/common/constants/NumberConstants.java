package org.fullguys.swingsynchronizer.common.constants;

public class NumberConstants {
    public static final int ZERO = 0;
    public static final int ONE = 1;

    public static final long LONG_ZERO = 0L;
}
