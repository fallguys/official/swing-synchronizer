package org.fullguys.swingsynchronizer.common.format;

import org.fullguys.swingsynchronizer.common.constants.StringConstants;

public class StringUtilsFormat {

    public static String fillWithChar(String word, char character, int length) {
        return String.format("%1$" + length + "s", word).replace(StringConstants.CHAR_SPACE, character);
    }
}
