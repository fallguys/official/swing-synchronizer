package org.fullguys.swingsynchronizer.components.database.models;

import lombok.Data;
import org.fullguys.swingsynchronizer.components.database.utils.Constants;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "MT_BENEFICIARY")
public class BeneficiaryModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beneficiary_id")
    private Long id;

    @Column(name = "district_id")
    private Long districtId;

    @Column(name = "code", length = Constants.BENEFICIARY_CODE_LENGTH)
    private String code;

    @Column(name = "is_older_adult")
    private Boolean isOlderAdult;

    @Column(name = "is_disabled")
    private Boolean isDisabled;

    @Column(name = "gender", length = Constants.BENEFICIARY_GENDER_LENGTH)
    private String gender;

    @Column(name = "state", length = Constants.BENEFICIARY_STATE_LENGTH)
    private String state;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    public interface CodeAndIdModel {
        Long getId();
        String getCode();
        LocalDateTime getCreatedAt();
    }
}
