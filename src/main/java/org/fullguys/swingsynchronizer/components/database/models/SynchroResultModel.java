package org.fullguys.swingsynchronizer.components.database.models;

import lombok.Data;
import org.fullguys.swingsynchronizer.components.database.utils.Constants;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ST_SYNCHRO_RESULT")
public class SynchroResultModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "synchro_result_id")
    private Long id;

    @Column(name = "synchro_id")
    private Long synchroId;

    @Column(name = "source_data_file", length = Constants.SYNCHRO_RESULT_NAME_FILE_LENGTH)
    private String sourceDataFile;

    @Column(name = "error_data_file", length = Constants.SYNCHRO_RESULT_NAME_FILE_LENGTH)
    private String errorDataFile;

    @Column(name = "total_registries")
    private Long totalRegistries;

    @Column(name = "successfull_registries")
    private Long successfulRegistries;

    @Column(name = "failed_registries")
    private Long failedRegistries;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "synchro_id", insertable = false, updatable = false)
    private SynchroModel synchro;
}
