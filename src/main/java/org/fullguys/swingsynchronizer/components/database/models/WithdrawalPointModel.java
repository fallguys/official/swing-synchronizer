package org.fullguys.swingsynchronizer.components.database.models;

import lombok.Data;
import org.fullguys.swingsynchronizer.components.database.utils.Constants;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@Entity
@Table(name = "MT_WITHDRAWAL_POINT")
public class WithdrawalPointModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "withdrawal_point_id")
    private Long id;

    @Column(name = "code", length = Constants.WITHDRAWAL_POINT_CODE_LENGTH)
    private String code;

    @Column(name = "name", length = Constants.WITHDRAWAL_POINT_NAME_LENGTH)
    private String name;

    @Column(name = "district_id")
    private Long districtId;

    @Column(name = "address", length = Constants.WITHDRAWAL_POINT_ADDRESS_LENGTH)
    private String address;

    @Column(name = "weekday_hour_start")
    private LocalTime weekdayHourStart;

    @Column(name = "weekday_hour_end")
    private LocalTime weekdayHourEnd;

    @Column(name = "weekend_hour_start")
    private LocalTime weekendHourStart;

    @Column(name = "weekend_hour_end")
    private LocalTime weekendHourEnd;

    @Column(name = "state", length = Constants.WITHDRAWAL_POINT_STATE_LENGTH)
    private String state;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    public interface CodeAndIDModel {
        Long getId();
        String getCode();
        LocalDateTime getCreatedAt();
    }
}
