package org.fullguys.swingsynchronizer.components.database.repository;

import org.fullguys.swingsynchronizer.components.database.models.BeneficiaryModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BeneficiaryRepository extends JpaRepository<BeneficiaryModel, Long> {
    List<BeneficiaryModel.CodeAndIdModel> findAllByCodeIn(List<String> codes);

    @Modifying
    @Query("UPDATE BeneficiaryModel benef SET benef.state = :state")
    void setAllBeneficiariesState(@Param("state") String state);
}
