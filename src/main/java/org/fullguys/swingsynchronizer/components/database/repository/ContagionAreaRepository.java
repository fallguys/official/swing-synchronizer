package org.fullguys.swingsynchronizer.components.database.repository;

import org.fullguys.swingsynchronizer.components.database.models.ContagionAreaModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContagionAreaRepository extends JpaRepository<ContagionAreaModel, Long> {
}
