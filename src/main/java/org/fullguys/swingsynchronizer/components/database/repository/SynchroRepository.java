package org.fullguys.swingsynchronizer.components.database.repository;

import org.fullguys.swingsynchronizer.components.database.models.SynchroModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SynchroRepository extends JpaRepository<SynchroModel, Long> {
    Optional<SynchroModel> findByState(String state);
}
