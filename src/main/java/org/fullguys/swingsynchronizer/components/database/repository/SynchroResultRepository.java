package org.fullguys.swingsynchronizer.components.database.repository;

import org.fullguys.swingsynchronizer.components.database.models.SynchroResultModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SynchroResultRepository extends JpaRepository<SynchroResultModel, Long> {
    Optional<SynchroResultModel> findBySynchroState(String state);
}
