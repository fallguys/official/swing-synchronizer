package org.fullguys.swingsynchronizer.components.database.repository;

import org.fullguys.swingsynchronizer.components.database.models.UbigeeModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UbigeeRepository extends JpaRepository<UbigeeModel, Long> {
    List<UbigeeModel> findByType(String type);
}
