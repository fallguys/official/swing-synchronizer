package org.fullguys.swingsynchronizer.components.database.repository;

import org.fullguys.swingsynchronizer.components.database.models.WithdrawalPointModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WithdrawalPointRepository extends JpaRepository<WithdrawalPointModel, Long> {
    List<WithdrawalPointModel.CodeAndIDModel> findAllByCodeIn(List<String> codes);

    @Modifying
    @Query("UPDATE WithdrawalPointModel point SET point.state = :state")
    void setAllPointsState(@Param("state") String state);

}
