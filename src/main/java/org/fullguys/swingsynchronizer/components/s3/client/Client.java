package org.fullguys.swingsynchronizer.components.s3.client;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.PersistenceAdapter;
import org.fullguys.swingsynchronizer.components.s3.config.StorageCredentials;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

@PersistenceAdapter
@Data
@RequiredArgsConstructor
public class Client {

    private final StorageCredentials storageCredentials;

    public AmazonS3 getS3Client(){
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(
                storageCredentials.getCredentials().getAccessKey(),
                storageCredentials.getCredentials().getSecretKey());

        return AmazonS3ClientBuilder.standard()
                .withRegion(storageCredentials.getRegion())
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    public InputStream downloadFile(String url) {
        try {
            AmazonS3URI amazonS3URI= new AmazonS3URI(new URI(url));
            var key = amazonS3URI.getKey();
            var request = new GetObjectRequest(storageCredentials.getBucket().getName(), key);
            var client = this.getS3Client();
            S3Object s3Object = client.getObject(request);

            return s3Object.getObjectContent();

        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

}
