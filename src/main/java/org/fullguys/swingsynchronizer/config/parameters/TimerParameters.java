package org.fullguys.swingsynchronizer.config.parameters;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter @Setter
@ConfigurationProperties(prefix = "synchronizer.timer")
public class TimerParameters {
    private String zone;
}
