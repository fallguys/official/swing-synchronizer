package org.fullguys.swingsynchronizer.domain.entities;

import lombok.Builder;
import lombok.Data;
import org.fullguys.swingsynchronizer.domain.entities.enums.BeneficiaryState;
import org.fullguys.swingsynchronizer.domain.entities.enums.Gender;

import java.time.LocalDateTime;

@Data
@Builder
public class Beneficiary {
    private Long id;
    private String code;
    private Long ubigeeID;

    private Boolean isOlderAdult;
    private Boolean isDisabled;
    private Gender gender;
    private BeneficiaryState state;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
