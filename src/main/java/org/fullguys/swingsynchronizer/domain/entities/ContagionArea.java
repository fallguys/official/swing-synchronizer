package org.fullguys.swingsynchronizer.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class ContagionArea {
    private Long id;
    private Long districtID;
    private Long quantity;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private Long population;
}
