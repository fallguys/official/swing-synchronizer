package org.fullguys.swingsynchronizer.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.fullguys.swingsynchronizer.domain.entities.enums.SynchroFileType;
import org.fullguys.swingsynchronizer.domain.entities.enums.SynchroState;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class Synchro {
    private Long id;
    private Long administratorId;
    private SynchroState state;
    private LocalDateTime initialDate;
    private SynchroFileType fileType;

    private Long resultId;
    private String dataFileURL;
    private String errorFileURL;
    private Long registriesTotal;
    private Long failedRegistriesTotal;
}
