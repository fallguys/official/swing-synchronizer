package org.fullguys.swingsynchronizer.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.fullguys.swingsynchronizer.domain.entities.enums.UbigeeType;

@Data
@Builder
@AllArgsConstructor
public class Ubigee {
    private Long id;
    private String code;
    private String name;
    private UbigeeType type;
}
