package org.fullguys.swingsynchronizer.domain.entities;

import lombok.Builder;
import lombok.Data;
import org.fullguys.swingsynchronizer.domain.entities.enums.WithdrawalPointState;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Data @Builder
public class WithdrawalPoint {

    private Long id;
    private String code;
    private String name;
    private String address;
    private LocalTime weekdayHourStart;
    private LocalTime weekdayHourEnd;
    private LocalTime weekendHourStart;
    private LocalTime weekendHourEnd;
    private WithdrawalPointState state;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    private Long districtID;
}
