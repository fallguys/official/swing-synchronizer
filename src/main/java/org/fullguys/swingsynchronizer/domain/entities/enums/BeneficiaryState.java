package org.fullguys.swingsynchronizer.domain.entities.enums;

public enum BeneficiaryState {
    ACTIVE,
    INACTIVE
}
