package org.fullguys.swingsynchronizer.domain.entities.enums;

public enum SynchroFileType {
    BENEFICIARY,
    WITHDRAWAL_POINT,
    CONTAGION_AREA
}
