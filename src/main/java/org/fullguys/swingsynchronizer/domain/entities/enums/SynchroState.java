package org.fullguys.swingsynchronizer.domain.entities.enums;

public enum SynchroState {
    IN_PROCESS,
    INCOMPLETE,
    FAILED,
    COMPLETED
}
