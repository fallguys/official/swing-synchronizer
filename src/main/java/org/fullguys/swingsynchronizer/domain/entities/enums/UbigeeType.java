package org.fullguys.swingsynchronizer.domain.entities.enums;

public enum UbigeeType {
    DEPARTMENT,
    PROVINCE,
    DISTRICT
}
