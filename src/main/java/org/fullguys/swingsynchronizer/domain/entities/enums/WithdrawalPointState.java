package org.fullguys.swingsynchronizer.domain.entities.enums;

public enum WithdrawalPointState {
    ACTIVE,
    INACTIVE
}
