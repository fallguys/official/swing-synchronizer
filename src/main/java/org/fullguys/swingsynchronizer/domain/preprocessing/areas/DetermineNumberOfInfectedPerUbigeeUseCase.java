package org.fullguys.swingsynchronizer.domain.preprocessing.areas;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.domain.entities.ContagionArea;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in.DetermineNumberOfInfectedPerUbigeeService;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.utils.ContagionAreasColumns;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@UseCase
@RequiredArgsConstructor
public class DetermineNumberOfInfectedPerUbigeeUseCase implements DetermineNumberOfInfectedPerUbigeeService {

    @Override
    public void determineNumberOfInfectedPerUbigee(DataFrame<Object> contagionPeopleDF, List<ContagionArea> contagionAreas) {

        var contagionAreasMapped = contagionAreas
                .stream()
                .peek(contagionArea -> contagionArea.setQuantity(NumberConstants.LONG_ZERO))
                .collect(Collectors.toMap(ContagionArea::getDistrictID, Function.identity()));

        contagionPeopleDF
                .groupBy(ContagionAreasColumns.CUSTOM_UBIGEE_ID)
                .explode()
                .forEach((ubigeeID, people) -> {
                    var population = (long) people.length();
                    contagionAreasMapped.get((Long) ubigeeID).setQuantity(population);
                });
    }
}
