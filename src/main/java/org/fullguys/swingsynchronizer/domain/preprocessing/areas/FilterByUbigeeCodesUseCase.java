package org.fullguys.swingsynchronizer.domain.preprocessing.areas;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.domain.entities.Ubigee;
import org.fullguys.swingsynchronizer.domain.entities.enums.UbigeeType;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in.FilterByUbigeeCodesService;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.utils.ContagionAreasColumns;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.GetDistrictsUbigeesService;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out.GetUbigeeByTypePort;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@UseCase
@RequiredArgsConstructor
public class FilterByUbigeeCodesUseCase implements FilterByUbigeeCodesService {

    private final GetUbigeeByTypePort getUbigeeByTypePort;
    private final GetDistrictsUbigeesService getDistrictsUbigeesService;

    @Override
    public DataFrame<Object> filterByUbigeeCodes(DataFrame<Object> contagionDF) {
        var ubigees = getDistrictsUbigeesService.getDistrictsUbigees()
                .stream()
                .collect(Collectors.toMap(Ubigee::getCode, Function.identity()));

        DataFrame.Predicate<Object> filterUbigee = (row) -> {
            var ubigeeCodeValue = (String) row.get(ContagionAreasColumns.INDEX_UBIGEE);
            return ubigees.containsKey(ubigeeCodeValue);
        };

        var contagionWithUbigeeIDs =  contagionDF.select(filterUbigee);

        IntStream.range(NumberConstants.ZERO, contagionWithUbigeeIDs.length())
                .forEach(index -> {
                    var ubigeeCode = (String) contagionWithUbigeeIDs.row(index).get(ContagionAreasColumns.INDEX_UBIGEE);
                    var ubigeeID = ubigees.get(ubigeeCode).getId();
                    contagionWithUbigeeIDs.set(index, ContagionAreasColumns.INDEX_CUSTOM_UBIGEE_ID, ubigeeID);
                });

        return contagionWithUbigeeIDs;
    }
}
