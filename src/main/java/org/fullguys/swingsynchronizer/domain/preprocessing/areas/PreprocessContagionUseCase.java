package org.fullguys.swingsynchronizer.domain.preprocessing.areas;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.adapters.file.ReaderAdapter;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.entities.Synchro;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in.DetermineNumberOfInfectedPerUbigeeService;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in.FilterByUbigeeCodesService;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in.PreprocessContagionService;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in.UpdateInfectedInContagionAreasService;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.out.GetContagionAreasPort;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.utils.ContagionDataframeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

@UseCase
@RequiredArgsConstructor
public class PreprocessContagionUseCase implements PreprocessContagionService {

    private final Logger logger = LoggerFactory.getLogger(PreprocessContagionUseCase.class);
    private final ReaderAdapter readerAdapter;
    private final ContagionDataframeManager contagionDataframeManager;

    private final FilterByUbigeeCodesService filterByUbigeeCodesService;
    private final DetermineNumberOfInfectedPerUbigeeService determineNumberOfInfectedPerUbigeeService;
    private final UpdateInfectedInContagionAreasService updateInfectedInContagionAreasService;

    private final GetContagionAreasPort getContagionAreasPort;

    @Override
    public Synchro preprocessContagion(InputStream file, Synchro synchro) throws IOException {
        logger.info("Start preprocessing contagion areas");
        var dataframe    = readerAdapter.readFile(file);
        var dataframeWithCustomColumns = contagionDataframeManager.createExpandedColumnDataframe(dataframe);

        updateCountProcessingSynchro(synchro, dataframeWithCustomColumns);

        logger.info(String.format("Contagion Areas - preprocessing %d people from contagion areas", dataframeWithCustomColumns.length()));
        var contagionPeopleWithUbigeesIDsDF = filterByUbigeeCodesService.filterByUbigeeCodes(dataframeWithCustomColumns);
        var contagionAreas = getContagionAreasPort.getContagionAreas();

        determineNumberOfInfectedPerUbigeeService.determineNumberOfInfectedPerUbigee(contagionPeopleWithUbigeesIDsDF, contagionAreas);
        updateInfectedInContagionAreasService.updateInfectedInContagionAreas(contagionAreas);

        return synchro;
    }

    private void updateCountProcessingSynchro(Synchro synchro, DataFrame<Object> contagionAreas){
        synchro.setRegistriesTotal((long) contagionAreas.length());
    }
}
