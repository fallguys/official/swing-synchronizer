package org.fullguys.swingsynchronizer.domain.preprocessing.areas;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.config.parameters.TimerParameters;
import org.fullguys.swingsynchronizer.domain.entities.ContagionArea;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in.UpdateInfectedInContagionAreasService;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.out.UpdateContagionAreasPort;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@UseCase
@RequiredArgsConstructor
public class UpdateInfectedInContagionAreasUseCase implements UpdateInfectedInContagionAreasService {

    private final TimerParameters timerParameters;
    private final UpdateContagionAreasPort updateContagionAreasPort;

    @Override
    public void updateInfectedInContagionAreas(List<ContagionArea> contagionAreas) {
        contagionAreas.forEach(contagionArea ->
                contagionArea.setUpdatedAt(LocalDateTime.now(ZoneId.of(timerParameters.getZone()))));

        updateContagionAreasPort.updateContagionAreas(contagionAreas);
    }
}
