package org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in;

import joinery.DataFrame;
import org.fullguys.swingsynchronizer.domain.entities.ContagionArea;

import java.util.List;

public interface DetermineNumberOfInfectedPerUbigeeService {
    void determineNumberOfInfectedPerUbigee(DataFrame<Object> contagionPeopleDF, List<ContagionArea> contagionAreas);

}
