package org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in;

import joinery.DataFrame;

public interface FilterByUbigeeCodesService {
    DataFrame<Object> filterByUbigeeCodes(DataFrame<Object> dataFrame);
}
