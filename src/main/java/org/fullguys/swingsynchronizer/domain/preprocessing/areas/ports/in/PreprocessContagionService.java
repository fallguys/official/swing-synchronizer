package org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.Synchro;

import java.io.IOException;
import java.io.InputStream;

public interface PreprocessContagionService {
    Synchro preprocessContagion(InputStream file, Synchro synchro) throws IOException;
}
