package org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.ContagionArea;

import java.util.List;

public interface UpdateInfectedInContagionAreasService {
    void updateInfectedInContagionAreas(List<ContagionArea> contagionAreas);
}
