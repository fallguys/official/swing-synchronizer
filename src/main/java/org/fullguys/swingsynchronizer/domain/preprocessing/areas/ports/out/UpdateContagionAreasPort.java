package org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.out;

import org.fullguys.swingsynchronizer.domain.entities.ContagionArea;

import java.util.List;

public interface UpdateContagionAreasPort {
    void updateContagionAreas(List<ContagionArea> contagionAreas);
}
