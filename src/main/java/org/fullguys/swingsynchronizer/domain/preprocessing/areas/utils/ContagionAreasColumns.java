package org.fullguys.swingsynchronizer.domain.preprocessing.areas.utils;

import java.util.Collection;
import java.util.List;

public class ContagionAreasColumns {

    // COLUMN NAMES
    public static final String CUTOFF_DATE  = "FECHA_CORTE";
    public static final String UUID         = "UUID";
    public static final String DEPARTMENT   = "DEPARTAMENTO";
    public static final String UBIGEE       = "UBIGEO";
    public static final String PROVINCE     = "PROVINCIA";
    public static final String DISTRICT     = "DISTRITO";
    public static final String METHOD       = "METODODX";
    public static final String AGE          = "EDAD";
    public static final String GENDER       = "SEXO";
    public static final String RESULT_DATE  = "FECHA_RESULTADO";

    public static final String CUSTOM_MESSAGE   = "MENSAJE DE ERROR";
    public static final String CUSTOM_UBIGEE_ID = "UBIGEE ID";

    // COLUMN INDEX
    public static final int INDEX_CUTOFF_DATE   = 0;
    public static final int INDEX_UUID          = 1;
    public static final int INDEX_DEPARTMENT    = 2;
    public static final int INDEX_UBIGEE        = 3;
    public static final int INDEX_PROVINCE      = 4;
    public static final int INDEX_DISTRICT      = 5;
    public static final int INDEX_METHOD        = 6;
    public static final int INDEX_AGE           = 7;
    public static final int INDEX_RESULT_DATE   = 8;

    public static final int INDEX_CUSTOM_MESSAGE    = 9;
    public static final int INDEX_CUSTOM_UBIGEE_ID  = 10;

    // LIST OF COLUMNS
    public static final Collection<String> ORIGINAL_COLUMNS = List.of(
            CUTOFF_DATE, UUID, DEPARTMENT, UBIGEE, PROVINCE, DISTRICT, METHOD, AGE, GENDER, RESULT_DATE
    );

    public static final Collection<String> ALL_COLLUMNS = List.of(
            CUTOFF_DATE, UUID, DEPARTMENT, UBIGEE, PROVINCE, DISTRICT, METHOD, AGE, GENDER, RESULT_DATE,
            CUSTOM_MESSAGE, CUSTOM_UBIGEE_ID
    );


}
