package org.fullguys.swingsynchronizer.domain.preprocessing.areas.utils;

import joinery.DataFrame;
import org.fullguys.swingsynchronizer.common.annotations.DataframeManager;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.common.constants.StringConstants;
import org.fullguys.swingsynchronizer.common.format.StringUtilsFormat;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.utils.UbigeeConstants;

import java.util.List;
import java.util.stream.IntStream;

@DataframeManager
public class ContagionDataframeManager {

    public DataFrame<Object> createExpandedColumnDataframe(DataFrame<Object> dataframe) {
        DataFrame.Function<List<Object>, Object> defaultUbigeeID  = row -> ContagionAreaConstants.DEFAULT_EMPTY_UBIGEE;

        var df = dataframe.add(ContagionAreasColumns.CUSTOM_UBIGEE_ID, defaultUbigeeID);
        return fixUbigeesWithZerosForCasting(df);
    }

    public DataFrame<Object> fixUbigeesWithZerosForCasting(DataFrame<Object> dataframe) {
        IntStream.range(NumberConstants.ZERO, dataframe.length())
                .forEach(index -> {
                    var ubigeeCode = dataframe.row(index).get(ContagionAreasColumns.INDEX_UBIGEE).toString();

                    var modified = StringUtilsFormat.fillWithChar(ubigeeCode,
                            StringConstants.CHAR_ZERO, UbigeeConstants.DISTRICT_UBIGEE_CODE_LENGTH);

                    dataframe.set(index, ContagionAreasColumns.INDEX_UBIGEE, modified);
                });

        return dataframe;
    }

    public DataFrame<Object> generateEmptyContagionDataframe() {
        return new DataFrame<>(ContagionAreasColumns.ALL_COLLUMNS);
    }
}
