package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.domain.entities.enums.Gender;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in.DetermineBeneficiariesGenderService;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils.BeneficiaryColumns;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils.BeneficiaryConstants;

import java.util.stream.IntStream;

@UseCase
@RequiredArgsConstructor
public class DetermineBeneficiariesGenderUseCase implements DetermineBeneficiariesGenderService {

    @Override
    public void determineBeneficiariesGender(DataFrame<Object> beneficiaries) {

        IntStream.range(NumberConstants.ZERO, beneficiaries.length())
                .forEach(index -> {
                    var gender = (long) beneficiaries.row(index).get(BeneficiaryColumns.INDEX_GENDER);
                    var genderValue =
                                gender == BeneficiaryConstants.MALE   ? Gender.MALE.name()
                            :   gender == BeneficiaryConstants.FEMALE ? Gender.FEMALE.name()
                            :   Gender.NO_ESPECIFIED.name();

                    beneficiaries.set(index, BeneficiaryColumns.INDEX_CUSTOM_GENDER, genderValue);
                });
    }
}
