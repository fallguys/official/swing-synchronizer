package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.domain.entities.Ubigee;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in.FilterByUbigeesCodesService;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils.BeneficiaryColumns;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.GetDistrictsUbigeesService;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@UseCase
@RequiredArgsConstructor
public class FilterByUbigeesCodesUseCase implements FilterByUbigeesCodesService {

    private final GetDistrictsUbigeesService getDistrictsUbigeesService;

    @Override
    public void filterByUbigeesCodes(DataFrame<Object> beneficiariesDF) {
        var ubigees = getDistrictsUbigeesService.getDistrictsUbigees()
                .stream()
                .collect(Collectors.toMap(Ubigee::getCode, Function.identity()));

        IntStream.range(NumberConstants.ZERO, beneficiariesDF.length())
                .forEach(index -> {
                    var ubigeeCode = (String) beneficiariesDF.row(index).get(BeneficiaryColumns.INDEX_UBIGEE);
                    if (ubigees.containsKey(ubigeeCode)) {
                        var ubigeeID = ubigees.get(ubigeeCode).getId();
                        beneficiariesDF.set(index, BeneficiaryColumns.INDEX_CUSTOM_UBIGEE_ID, ubigeeID);
                    }
                });
    }
}
