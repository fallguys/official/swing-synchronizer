package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in.GetChosenBeneficiariesService;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils.BenefDataframeManager;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils.BeneficiaryColumns;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils.BeneficiaryConstants;

import java.util.Collection;

@UseCase
@RequiredArgsConstructor
public class GetChosenBeneficiariesUseCase implements GetChosenBeneficiariesService {

    private final BenefDataframeManager benefDataframeManager;

    @Override
    public DataFrame<Object> getChosenBeneficiaries(Collection<DataFrame<Object>> families) {
        var emptyDataframe = benefDataframeManager.generateEmptyBeneficiariesDataframe();

        return families.stream()
                .filter(this::filterFamilyWithoutChosenBeneficiary)
                .map(this::getChosenFromFamily)
                .reduce(emptyDataframe, this::mergeChosenOnes);
    }

    private boolean filterFamilyWithoutChosenBeneficiary(DataFrame<Object> family) {
        var chosenCount = family.col(BeneficiaryColumns.INDEX_CHOSEN)
                .stream()
                .filter(chosen -> (Long) chosen == BeneficiaryConstants.CHOSEN)
                .count();

        return chosenCount == BeneficiaryConstants.MAX_BENEFS_IN_FAMILY;
    }

    private DataFrame<Object> getChosenFromFamily(DataFrame<Object> family) {
        DataFrame.Predicate<Object> onlyChosen = (row) -> {
            var chosenValue = (Long) row.get(BeneficiaryColumns.INDEX_CHOSEN);
            return chosenValue == BeneficiaryConstants.CHOSEN;
        };

        return family.select(onlyChosen);
    }

    private DataFrame<Object> mergeChosenOnes(DataFrame<Object> merged, DataFrame<Object> chosen) {
        var emptyDataframe = benefDataframeManager.generateEmptyBeneficiariesDataframe();
        merged.forEach(emptyDataframe::append);
        chosen.forEach(emptyDataframe::append);
        return emptyDataframe;
    }
}
