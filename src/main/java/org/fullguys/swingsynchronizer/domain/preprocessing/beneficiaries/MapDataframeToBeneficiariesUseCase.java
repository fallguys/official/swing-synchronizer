package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.domain.entities.Beneficiary;
import org.fullguys.swingsynchronizer.domain.entities.enums.Gender;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in.MapDataframeToBeneficiariesService;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils.BeneficiaryColumns;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@UseCase
@RequiredArgsConstructor
public class MapDataframeToBeneficiariesUseCase implements MapDataframeToBeneficiariesService {

    @Override
    public List<Beneficiary> mapDataframeToBeneficiaries(DataFrame<Object> beneficiariesDF) {
        return IntStream.range(NumberConstants.ZERO, beneficiariesDF.length())
                .mapToObj(index -> {
                    var row = beneficiariesDF.row(index);
                    return Beneficiary.builder()
                            .code((String) row.get(BeneficiaryColumns.INDEX_CODE))
                            .gender(Gender.valueOf((String) row.get(BeneficiaryColumns.INDEX_CUSTOM_GENDER)))
                            .ubigeeID((Long) row.get(BeneficiaryColumns.INDEX_CUSTOM_UBIGEE_ID))
                            .isDisabled((Boolean) row.get(BeneficiaryColumns.INDEX_CUSTOM_DISABLED))
                            .isOlderAdult((Boolean) row.get(BeneficiaryColumns.INDEX_CUSTOM_OLDER))
                            .build();
                })
                .collect(Collectors.toList());
    }
}
