package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.entities.Beneficiary;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in.MatchBeneficiariesIDsService;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.out.GetBeneficiariesIDByCodesPort;

import java.util.List;
import java.util.stream.Collectors;

@UseCase
@RequiredArgsConstructor
public class MatchBeneficiariesIDsUseCase implements MatchBeneficiariesIDsService {

    private final GetBeneficiariesIDByCodesPort getBeneficiariesIDByCodesPort;

    @Override
    public void matchBeneficiariesIDs(List<Beneficiary> beneficiaries) {
        var beneficiariesCodes = beneficiaries.stream()
                .map(Beneficiary::getCode)
                .collect(Collectors.toList());

        var beneficiariesIDs = getBeneficiariesIDByCodesPort.getBeneficiariesIDByCodes(beneficiariesCodes);

        beneficiaries.forEach(beneficiary -> {
            var code = beneficiary.getCode();

            if (beneficiariesIDs.containsKey(code)) {
                var benefID = beneficiariesIDs.get(code).getValue0();
                var createdAt = beneficiariesIDs.get(code).getValue1();
                beneficiary.setId(benefID);
                beneficiary.setCreatedAt(createdAt);
            }
        });
    }
}
