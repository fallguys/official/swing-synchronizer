package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.adapters.file.ReaderAdapter;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.domain.entities.Synchro;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in.*;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils.BenefDataframeManager;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils.BeneficiaryColumns;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

@UseCase
@RequiredArgsConstructor
public class PreprocessBeneficiariesUseCase implements PreprocessBeneficiariesService {

    private final Logger logger = LoggerFactory.getLogger(PreprocessBeneficiariesUseCase.class);
    private final ReaderAdapter readerAdapter;
    private final BenefDataframeManager benefDataframeManager;

    private final GetChosenBeneficiariesService getChosenBeneficiariesService;

    private final FilterByUbigeesCodesService filterByUbigeesCodesService;
    private final DetermineBeneficiariesGenderService determineBeneficiariesGenderService;
    private final DetermineBeneficiariesOlderService determineBeneficiariesOlderService;
    private final DetermineBeneficiariesDisabledService determineBeneficiariesDisabledService;

    private final MapDataframeToBeneficiariesService mapDataframeToBeneficiariesService;
    private final MatchBeneficiariesIDsService matchBeneficiariesIDsService;
    private final SetDefaultPreprocessedBeneficiaryValuesService setDefaultPreprocessedBeneficiaryValuesService;

    private final RegisterPreprocessedBeneficiariesService registerPreprocessedBeneficiariesService;

    @Override
    public Synchro preprocessBeneficiaries(InputStream file, Synchro synchro) throws IOException {
        logger.info("Start preprocessing beneficiaries");

        var dataframe = readerAdapter.readFile(file);
        var dataframeWithCustomColumns = benefDataframeManager.createExpandedColumnDataframe(dataframe);

        logger.info("Beneficiaries - preprocessing families");

        var families = separateInFamilies(dataframeWithCustomColumns);
        var chosenBeneficiaries = getChosenBeneficiariesService.getChosenBeneficiaries(families);

        updateCountProcessingSynchro(synchro, chosenBeneficiaries);

        logger.info(String.format("Beneficiaries - preprocessing %d beneficiaries and their attributes", chosenBeneficiaries.length()));

        filterByUbigeesCodesService.filterByUbigeesCodes(chosenBeneficiaries);
        determineBeneficiariesGenderService.determineBeneficiariesGender(chosenBeneficiaries);
        determineBeneficiariesOlderService.determineBeneficiariesOlder(chosenBeneficiaries);
        determineBeneficiariesDisabledService.determineBeneficiariesDisabled(chosenBeneficiaries);

        logger.info(String.format("Beneficiaries - mapping %d beneficiaries with current ones", chosenBeneficiaries.length()));

        var mappedBeneficiaries = mapDataframeToBeneficiariesService.mapDataframeToBeneficiaries(chosenBeneficiaries);

        matchBeneficiariesIDsService.matchBeneficiariesIDs(mappedBeneficiaries);
        setDefaultPreprocessedBeneficiaryValuesService.setDefaultPreprocessedBeneficiaryValues(mappedBeneficiaries);

        logger.info(String.format("Beneficiaries - registering %d beneficiaries", mappedBeneficiaries.size()));

        registerPreprocessedBeneficiariesService.registerPreprocessedBeneficiaries(mappedBeneficiaries);

        updateCountErrorSynchro(synchro);
        return synchro;
    }

    private Collection<DataFrame<Object>> separateInFamilies(DataFrame<Object> beneficiariesDF) {
        return beneficiariesDF
                .groupBy(BeneficiaryColumns.CODE)
                .explode()
                .values();
    }

    private void updateCountProcessingSynchro(Synchro synchro, DataFrame<Object> beneficiaries) {
        synchro.setRegistriesTotal((long)beneficiaries.length());
    }

    private void updateCountErrorSynchro(Synchro synchro) {
        synchro.setFailedRegistriesTotal((long)NumberConstants.ZERO);
    }
}
