package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.entities.Beneficiary;
import org.fullguys.swingsynchronizer.domain.entities.enums.BeneficiaryState;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in.RegisterPreprocessedBeneficiariesService;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.out.SaveBeneficiariesPort;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.out.UpdateBeneficiaryStatePort;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class RegisterPreprocessedBeneficiariesUseCase implements RegisterPreprocessedBeneficiariesService {

    private final UpdateBeneficiaryStatePort updateBeneficiaryStatePort;
    private final SaveBeneficiariesPort saveBeneficiariesPort;

    @Override
    public void registerPreprocessedBeneficiaries(List<Beneficiary> beneficiaries) {
        disabledAllCurrentBeneficiaries();
        saveBeneficiariesPort.saveBeneficiaries(beneficiaries);
    }

    private void disabledAllCurrentBeneficiaries() {
        updateBeneficiaryStatePort.updateBeneficiaryState(BeneficiaryState.INACTIVE);
    }
}
