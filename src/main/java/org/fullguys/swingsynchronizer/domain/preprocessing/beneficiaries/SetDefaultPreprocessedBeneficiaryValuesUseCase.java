package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.config.parameters.TimerParameters;
import org.fullguys.swingsynchronizer.domain.entities.Beneficiary;
import org.fullguys.swingsynchronizer.domain.entities.enums.BeneficiaryState;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in.SetDefaultPreprocessedBeneficiaryValuesService;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@UseCase
@RequiredArgsConstructor
public class SetDefaultPreprocessedBeneficiaryValuesUseCase implements SetDefaultPreprocessedBeneficiaryValuesService {

    private final TimerParameters timerParameters;

    @Override
    public void setDefaultPreprocessedBeneficiaryValues(List<Beneficiary> beneficiaries) {
        beneficiaries.forEach( beneficiary -> {
            setState(beneficiary);
            setCreatedAt(beneficiary);
            setUpdatedAt(beneficiary);
        });
    }

    private void setState(Beneficiary beneficiary) {
        beneficiary.setState(BeneficiaryState.ACTIVE);
    }

    private void setCreatedAt(Beneficiary beneficiary) {
        if (beneficiary.getId() == null) {
            beneficiary.setCreatedAt(LocalDateTime.now(ZoneId.of(timerParameters.getZone())));
        }
    }

    private void setUpdatedAt(Beneficiary beneficiary) {
        if (beneficiary.getId() != null) {
            beneficiary.setUpdatedAt(LocalDateTime.now(ZoneId.of(timerParameters.getZone())));
        }
    }
}
