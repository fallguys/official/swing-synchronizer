package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in;

import joinery.DataFrame;

public interface DetermineBeneficiariesDisabledService {
    void determineBeneficiariesDisabled(DataFrame<Object> beneficiaries);
}
