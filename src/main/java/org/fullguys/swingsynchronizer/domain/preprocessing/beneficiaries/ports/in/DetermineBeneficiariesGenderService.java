package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in;

import joinery.DataFrame;

public interface DetermineBeneficiariesGenderService {
    void determineBeneficiariesGender(DataFrame<Object> beneficiaries);
}
