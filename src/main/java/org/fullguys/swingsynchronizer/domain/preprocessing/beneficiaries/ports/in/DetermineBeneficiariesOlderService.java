package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in;

import joinery.DataFrame;

public interface DetermineBeneficiariesOlderService {
    void determineBeneficiariesOlder(DataFrame<Object> beneficiaries);
}
