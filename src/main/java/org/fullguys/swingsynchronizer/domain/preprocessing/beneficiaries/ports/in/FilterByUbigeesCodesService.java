package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in;

import joinery.DataFrame;

public interface FilterByUbigeesCodesService {
    void filterByUbigeesCodes(DataFrame<Object> beneficiariesDF);
}
