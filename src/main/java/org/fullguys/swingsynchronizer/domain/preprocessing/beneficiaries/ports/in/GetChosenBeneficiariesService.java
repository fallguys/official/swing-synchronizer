package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in;

import joinery.DataFrame;

import java.util.Collection;

public interface GetChosenBeneficiariesService {
    DataFrame<Object> getChosenBeneficiaries(Collection<DataFrame<Object>> families);
}
