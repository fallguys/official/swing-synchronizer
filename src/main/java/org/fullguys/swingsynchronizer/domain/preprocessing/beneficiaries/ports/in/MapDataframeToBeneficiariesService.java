package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in;

import joinery.DataFrame;
import org.fullguys.swingsynchronizer.domain.entities.Beneficiary;

import java.util.List;

public interface MapDataframeToBeneficiariesService {
    List<Beneficiary> mapDataframeToBeneficiaries(DataFrame<Object> beneficiariesDF);
}
