package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.Beneficiary;

import java.util.List;

public interface MatchBeneficiariesIDsService {
    void matchBeneficiariesIDs(List<Beneficiary> beneficiaries);
}
