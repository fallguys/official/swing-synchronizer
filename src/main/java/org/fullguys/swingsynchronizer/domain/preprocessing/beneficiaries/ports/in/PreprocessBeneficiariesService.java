package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.Synchro;

import java.io.IOException;
import java.io.InputStream;

public interface PreprocessBeneficiariesService {
    Synchro preprocessBeneficiaries(InputStream file, Synchro synchro) throws IOException;
}
