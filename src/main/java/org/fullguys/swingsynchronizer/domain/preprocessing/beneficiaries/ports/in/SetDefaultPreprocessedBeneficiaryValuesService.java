package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.Beneficiary;

import java.util.List;

public interface SetDefaultPreprocessedBeneficiaryValuesService {
    void setDefaultPreprocessedBeneficiaryValues(List<Beneficiary> beneficiaries);
}
