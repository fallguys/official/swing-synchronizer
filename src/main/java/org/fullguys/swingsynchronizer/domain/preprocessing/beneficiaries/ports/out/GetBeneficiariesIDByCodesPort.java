package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.out;

import org.javatuples.Pair;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface GetBeneficiariesIDByCodesPort {
    Map<String, Pair<Long, LocalDateTime>> getBeneficiariesIDByCodes(List<String> beneficiariesCodes);
}
