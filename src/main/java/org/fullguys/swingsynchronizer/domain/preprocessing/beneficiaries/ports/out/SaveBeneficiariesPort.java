package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.out;

import org.fullguys.swingsynchronizer.domain.entities.Beneficiary;

import java.util.List;

public interface SaveBeneficiariesPort {
    void saveBeneficiaries(List<Beneficiary> beneficiaries);
}
