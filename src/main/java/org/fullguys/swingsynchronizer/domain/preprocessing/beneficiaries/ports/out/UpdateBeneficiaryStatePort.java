package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.out;

import org.fullguys.swingsynchronizer.domain.entities.enums.BeneficiaryState;

public interface UpdateBeneficiaryStatePort {
    void updateBeneficiaryState(BeneficiaryState state);
}
