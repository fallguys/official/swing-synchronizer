package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils;

import joinery.DataFrame;
import org.fullguys.swingsynchronizer.common.annotations.DataframeManager;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.common.constants.StringConstants;
import org.fullguys.swingsynchronizer.common.format.StringUtilsFormat;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.utils.UbigeeConstants;

import java.util.List;
import java.util.stream.IntStream;

@DataframeManager
public class BenefDataframeManager {

    public DataFrame<Object> createExpandedColumnDataframe(DataFrame<Object> dataframe) {
        DataFrame.Function<List<Object>, Object> defaultMessage   = row -> BeneficiaryConstants.DEFAULT_EMPTY_MESSAGE;
        DataFrame.Function<List<Object>, Object> defaultGender    = row -> BeneficiaryConstants.DEFAULT_EMPTY_GENDER;
        DataFrame.Function<List<Object>, Object> defaultUbigeeID  = row -> BeneficiaryConstants.DEFAULT_EMPTY_UBIGEE;

        var df = dataframe
                .add(BeneficiaryColumns.CUSTOM_MESSAGE, defaultMessage)
                .add(BeneficiaryColumns.CUSTOM_GENDER, defaultGender)
                .add(BeneficiaryColumns.CUSTOM_UBIGEE_ID, defaultUbigeeID)
                .convert(
                        BeneficiaryColumns.TYPE_CODE,
                        BeneficiaryColumns.TYPE_UBIGEE,
                        BeneficiaryColumns.TYPE_DEPARTMENT,
                        BeneficiaryColumns.TYPE_PROVINCE,
                        BeneficiaryColumns.TYPE_DISTRICT,
                        BeneficiaryColumns.TYPE_GENDER,
                        BeneficiaryColumns.TYPE_RESTRICTION,
                        BeneficiaryColumns.TYPE_CHOSEN,
                        BeneficiaryColumns.TYPE_DISABLED,
                        BeneficiaryColumns.TYPE_ELDER,
                        BeneficiaryColumns.TYPE_CUSTOM_MESSAGE,
                        BeneficiaryColumns.TYPE_CUSTOM_GENDER,
                        BeneficiaryColumns.TYPE_CUSTOM_UBIGEE_ID
                );

        return fixUbigeesWithZerosForCasting(df);
    }

    public DataFrame<Object> fixUbigeesWithZerosForCasting(DataFrame<Object> dataframe) {
        IntStream.range(NumberConstants.ZERO, dataframe.length())
                .forEach(index -> {
                    var ubigeeCode = (String) dataframe.row(index).get(BeneficiaryColumns.INDEX_UBIGEE);

                    var modified = StringUtilsFormat.fillWithChar(ubigeeCode,
                            StringConstants.CHAR_ZERO, UbigeeConstants.DISTRICT_UBIGEE_CODE_LENGTH);

                    dataframe.set(index, BeneficiaryColumns.INDEX_UBIGEE, modified);
                });

        return dataframe;
    }

    public DataFrame<Object> generateEmptyBeneficiariesDataframe() {
        return new DataFrame<>(BeneficiaryColumns.ALL_COLUMNS)
                .convert(
                        BeneficiaryColumns.TYPE_CODE,
                        BeneficiaryColumns.TYPE_UBIGEE,
                        BeneficiaryColumns.TYPE_DEPARTMENT,
                        BeneficiaryColumns.TYPE_PROVINCE,
                        BeneficiaryColumns.TYPE_DISTRICT,
                        BeneficiaryColumns.TYPE_GENDER,
                        BeneficiaryColumns.TYPE_RESTRICTION,
                        BeneficiaryColumns.TYPE_CHOSEN,
                        BeneficiaryColumns.TYPE_DISABLED,
                        BeneficiaryColumns.TYPE_ELDER,
                        BeneficiaryColumns.TYPE_CUSTOM_MESSAGE,
                        BeneficiaryColumns.TYPE_CUSTOM_GENDER,
                        BeneficiaryColumns.TYPE_CUSTOM_UBIGEE_ID
                );
    }
}
