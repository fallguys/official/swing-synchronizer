package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils;

import java.util.Collection;
import java.util.List;

public class BeneficiaryColumns {

    // COLUMN NAMES
    public static final String CODE         = "CO_HOGAR";
    public static final String UBIGEE       = "UBIGEO";
    public static final String DEPARTMENT   = "DE_DEPARTAMENTO";
    public static final String PROVINCE     = "DE_PROVINCIA";
    public static final String DISTRICT     = "DE_DISTRITO";
    public static final String GENDER       = "DE_GENERO";
    public static final String RESTRICTION  = "CO_RESTRI";
    public static final String CHOSEN       = "FLAG_PADRON_OLD";
    public static final String DISABLED     = "FLAG_DISCAP_SEVERA";
    public static final String ELDER        = "FLAG_MAYEDAD";

    public static final String CUSTOM_MESSAGE   = "MENSAJE DE ERROR";
    public static final String CUSTOM_GENDER    = "GENDER";
    public static final String CUSTOM_UBIGEE_ID = "DISTRICT UBIGEE ID";
    public static final String CUSTOM_ELDER     = "IS OLD ADULT";
    public static final String CUSTOM_DISABLED  = "IS DISABLED";

    // COLUMN INDEX
    public static final int INDEX_CODE          = 0;
    public static final int INDEX_UBIGEE        = 1;
    public static final int INDEX_DEPARTMENT    = 2;
    public static final int INDEX_PROVINCE      = 3;
    public static final int INDEX_DISTRICT      = 4;
    public static final int INDEX_GENDER        = 5;
    public static final int INDEX_RESTRICTION   = 6;
    public static final int INDEX_CHOSEN        = 7;
    public static final int INDEX_DISABLED      = 8;
    public static final int INDEX_ELDER         = 9;

    public static final int INDEX_CUSTOM_MESSAGE    = 10;
    public static final int INDEX_CUSTOM_GENDER     = 11;
    public static final int INDEX_CUSTOM_UBIGEE_ID  = 12;
    public static final int INDEX_CUSTOM_OLDER      = 13;
    public static final int INDEX_CUSTOM_DISABLED   = 14;

    // COLUMN TYPES
    public static final Class<String> TYPE_CODE         = String.class;
    public static final Class<String> TYPE_UBIGEE       = String.class;
    public static final Class<String> TYPE_DEPARTMENT   = String.class;
    public static final Class<String> TYPE_PROVINCE     = String.class;
    public static final Class<String> TYPE_DISTRICT     = String.class;
    public static final Class<Long>   TYPE_GENDER       = Long.class;
    public static final Class<Long>   TYPE_RESTRICTION  = Long.class;
    public static final Class<Long>   TYPE_CHOSEN       = Long.class;
    public static final Class<Long>   TYPE_DISABLED     = Long.class;
    public static final Class<Long>   TYPE_ELDER        = Long.class;

    public static final Class<String>  TYPE_CUSTOM_MESSAGE   = String.class;
    public static final Class<Long>    TYPE_CUSTOM_GENDER    = Long.class;
    public static final Class<Long>    TYPE_CUSTOM_UBIGEE_ID = Long.class;
    public static final Class<Boolean> TYPE_CUSTOM_ELDER     = Boolean.class;
    public static final Class<Boolean> TYPE_CUSTOM_DISABLED  = Boolean.class;

    // LIST OF ORIGINAL COLUMN NAMES
    public static final Collection<String> ORIGINAL_COLUMNS = List.of(
            CODE, UBIGEE, DEPARTMENT, PROVINCE, DISTRICT, GENDER, RESTRICTION, CHOSEN, DISABLED, ELDER
    );

    // LIST OF ORIGINAL COLUMN TYPES
    public static final Collection<Class<?>> COLUMNS_TYPES = List.of(
            TYPE_CODE, TYPE_UBIGEE, TYPE_DEPARTMENT, TYPE_PROVINCE, TYPE_DISTRICT, TYPE_GENDER, TYPE_RESTRICTION, TYPE_CHOSEN, TYPE_DISABLED, TYPE_ELDER
    );

    // LIST OF COLUMN NAMES
    public static final Collection<Class<?>> ALL_COLUMNS_TYPES = List.of(
            TYPE_CODE, TYPE_UBIGEE, TYPE_DEPARTMENT, TYPE_PROVINCE, TYPE_DISTRICT, TYPE_GENDER, TYPE_RESTRICTION, TYPE_CHOSEN, TYPE_DISABLED, TYPE_ELDER,
            TYPE_CUSTOM_MESSAGE, TYPE_CUSTOM_GENDER, TYPE_CUSTOM_UBIGEE_ID, TYPE_CUSTOM_ELDER, TYPE_CUSTOM_DISABLED
    );

    // LIST OF COLUMN TYPES
    public static final Collection<String> ALL_COLUMNS = List.of(
            CODE, UBIGEE, DEPARTMENT, PROVINCE, DISTRICT, GENDER, RESTRICTION, CHOSEN, DISABLED, ELDER,
            CUSTOM_MESSAGE, CUSTOM_GENDER, CUSTOM_UBIGEE_ID, CUSTOM_ELDER, CUSTOM_DISABLED
    );
}
