package org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.utils;

import org.fullguys.swingsynchronizer.domain.entities.enums.Gender;

public class BeneficiaryConstants {
    public static final long CHOSEN = 1L;
    public static final long MAX_BENEFS_IN_FAMILY = 1L;

    public static final long FEMALE = 2;
    public static final long MALE = 1;

    public static final String DEFAULT_EMPTY_MESSAGE = "";
    public static final long DEFAULT_EMPTY_UBIGEE = 0L;
    public static final String DEFAULT_EMPTY_GENDER = Gender.NO_ESPECIFIED.name();

    public static final long IS_OLDER = 1L;
    public static final long IS_DISABLED = 1L;
}
