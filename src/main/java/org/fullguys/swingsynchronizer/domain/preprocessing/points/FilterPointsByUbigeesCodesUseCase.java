package org.fullguys.swingsynchronizer.domain.preprocessing.points;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.domain.entities.Ubigee;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in.FilterPointsByUbigeesCodesService;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.utils.PointColumns;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.GetDistrictsUbigeesService;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@UseCase
@RequiredArgsConstructor
public class FilterPointsByUbigeesCodesUseCase implements FilterPointsByUbigeesCodesService {

    private final GetDistrictsUbigeesService getDistrictsUbigeesService;

    @Override
    public void filterPointsByUbigeesCodes(DataFrame<Object> pointsDF) {
        var ubigees = getDistrictsUbigeesService.getDistrictsUbigees()
                .stream()
                .collect(Collectors.toMap(Ubigee::getCode, Function.identity()));

        IntStream.range(NumberConstants.ZERO, pointsDF.length())
                .forEach(index -> {
                    var ubigeeCode = (String) pointsDF.row(index).get(PointColumns.INDEX_UBIGEE);
                    if (ubigees.containsKey(ubigeeCode)) {
                        var ubigeeID = ubigees.get(ubigeeCode).getId();
                        pointsDF.set(index, PointColumns.INDEX_CUSTOM_UBIGEE_ID, ubigeeID);
                    }
                });
    }
}
