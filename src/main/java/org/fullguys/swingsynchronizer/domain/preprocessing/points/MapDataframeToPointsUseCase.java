package org.fullguys.swingsynchronizer.domain.preprocessing.points;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.domain.entities.WithdrawalPoint;
import org.fullguys.swingsynchronizer.domain.entities.enums.WithdrawalPointState;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in.MapDataframeToPointsService;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.utils.PointColumns;

import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@UseCase
@RequiredArgsConstructor
public class MapDataframeToPointsUseCase implements MapDataframeToPointsService {

    @Override
    public List<WithdrawalPoint> mapDataframeToPoints(DataFrame<Object> pointsDF) {
        return IntStream.range(NumberConstants.ZERO, pointsDF.length())
                .mapToObj(index -> {
                    var row = pointsDF.row(index);

                    return WithdrawalPoint.builder()
                            .code       (row.get(PointColumns.INDEX_CODE).toString())
                            .address    ((String) row.get(PointColumns.INDEX_ADDRESS))
                            .name       ((String) row.get(PointColumns.INDEX_NAME))
                            .districtID ((Long  ) row.get(PointColumns.INDEX_CUSTOM_UBIGEE_ID))
                            .weekdayHourStart   (LocalTime.parse((String) row.get(PointColumns.INDEX_HOUR_INIT_WEEKDAYS)))
                            .weekdayHourEnd     (LocalTime.parse((String) row.get(PointColumns.INDEX_HOUR_END_WEEKDAYS)))
                            .weekendHourStart   (LocalTime.parse((String) row.get(PointColumns.INDEX_HOUR_INIT_SATURDAY)))
                            .weekendHourEnd     (LocalTime.parse((String) row.get(PointColumns.INDEX_HOUR_END_SATURDAY)))
                            .state(WithdrawalPointState.ACTIVE)
                            .build();
                })
                .collect(Collectors.toList());
    }
}
