package org.fullguys.swingsynchronizer.domain.preprocessing.points;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.config.parameters.TimerParameters;
import org.fullguys.swingsynchronizer.domain.entities.WithdrawalPoint;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in.MatchPointsIDsService;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.out.GetPointsIDByCodesPort;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@UseCase
@RequiredArgsConstructor
public class MatchPointsIDsUseCase implements MatchPointsIDsService {

    private final GetPointsIDByCodesPort getPointsIDByCodesPort;
    private final TimerParameters timerParameters;

    @Override
    public void matchPointsIDs(List<WithdrawalPoint> points) {

        var pointsCodes = points
                .stream()
                .map(WithdrawalPoint::getCode)
                .collect(Collectors.toList());

        var pointsIDs = getPointsIDByCodesPort.getPointsIDByCodes(pointsCodes);
        var timeNow = LocalDateTime.now(ZoneId.of(timerParameters.getZone()));

        points.forEach(point -> {
            var code = point.getCode();

            if (pointsIDs.containsKey(code)) {
                var pointID = pointsIDs.get(code).getValue0();
                var createdAt = pointsIDs.get(code).getValue1();

                point.setId(pointID);
                point.setCreatedAt(createdAt);

                point.setUpdatedAt(timeNow);
                return;
            }

            point.setCreatedAt(timeNow);
        });
    }
}
