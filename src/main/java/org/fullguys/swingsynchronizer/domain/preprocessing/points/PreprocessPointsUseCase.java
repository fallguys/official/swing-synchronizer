package org.fullguys.swingsynchronizer.domain.preprocessing.points;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.adapters.file.ReaderAdapter;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.entities.Synchro;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in.*;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.utils.PointDataframeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

@UseCase
@RequiredArgsConstructor
public class PreprocessPointsUseCase implements PreprocessPointsService {

    private final Logger logger = LoggerFactory.getLogger(PreprocessPointsUseCase.class);
    private final ReaderAdapter readerAdapter;
    private final PointDataframeManager pointDataframeManager;

    private final FilterPointsByUbigeesCodesService filterByUbigeesCodesService;
    private final MapDataframeToPointsService mapDataframeToPointsService;
    private final MatchPointsIDsService matchPointsIDsService;
    private final SavePointsService savePointsService;

    @Override
    public Synchro preprocessPoints(InputStream file, Synchro synchro) throws IOException {
        logger.info("Withdrawal Points - Start preprocessing points");

        var dataframe = readerAdapter.readFile(file);
        var pointsDF = pointDataframeManager.createExpandedColumnDataframe(dataframe);

        updateCountProcessingSynchro(synchro, pointsDF);
        filterByUbigeesCodesService.filterPointsByUbigeesCodes(pointsDF);

        logger.info(String.format("Withdrawal Points - mapping %d points with current ones", pointsDF.length()));
        var points = mapDataframeToPointsService.mapDataframeToPoints(pointsDF);
        matchPointsIDsService.matchPointsIDs(points);

        logger.info(String.format("Withdrawal Points - saving %d points", pointsDF.length()));
        savePointsService.savePoints(points);

        return synchro;
    }

    private void updateCountProcessingSynchro(Synchro synchro, DataFrame<Object> dataframeWithCustomColumns) {
        synchro.setRegistriesTotal((long) dataframeWithCustomColumns.length());
    }
}
