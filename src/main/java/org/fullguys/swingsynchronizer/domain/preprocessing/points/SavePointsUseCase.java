package org.fullguys.swingsynchronizer.domain.preprocessing.points;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.entities.WithdrawalPoint;
import org.fullguys.swingsynchronizer.domain.entities.enums.WithdrawalPointState;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in.SavePointsPort;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in.SavePointsService;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.out.UpdatePointsStatePort;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class SavePointsUseCase implements SavePointsService {

    private final UpdatePointsStatePort updatePointsStatePort;
    private final SavePointsPort savePointsPort;

    @Override
    public void savePoints(List<WithdrawalPoint> points) {
        disabledAllCurrentPoints();
        savePointsPort.savePoints(points);
    }

    private void disabledAllCurrentPoints() {
        updatePointsStatePort.updatePointsState(WithdrawalPointState.INACTIVE);
    }
}
