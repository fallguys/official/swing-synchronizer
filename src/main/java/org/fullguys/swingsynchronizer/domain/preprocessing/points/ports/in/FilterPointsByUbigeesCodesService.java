package org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in;

import joinery.DataFrame;

public interface FilterPointsByUbigeesCodesService {
    void filterPointsByUbigeesCodes(DataFrame<Object> pointsDF);
}
