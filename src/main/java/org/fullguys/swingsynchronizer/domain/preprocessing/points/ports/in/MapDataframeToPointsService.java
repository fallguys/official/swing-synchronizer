package org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in;

import joinery.DataFrame;
import org.fullguys.swingsynchronizer.domain.entities.WithdrawalPoint;

import java.util.List;

public interface MapDataframeToPointsService {
    List<WithdrawalPoint> mapDataframeToPoints(DataFrame<Object> pointsDF);
}
