package org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.Synchro;

import java.io.IOException;
import java.io.InputStream;

public interface PreprocessPointsService {
    Synchro preprocessPoints(InputStream file, Synchro synchro) throws IOException;
}
