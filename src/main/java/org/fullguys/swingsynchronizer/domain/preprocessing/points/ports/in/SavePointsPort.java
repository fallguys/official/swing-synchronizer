package org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.WithdrawalPoint;

import java.util.List;

public interface SavePointsPort {
    void savePoints(List<WithdrawalPoint> points);
}
