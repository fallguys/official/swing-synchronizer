package org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.WithdrawalPoint;

import java.util.List;

public interface SavePointsService {
    void savePoints(List<WithdrawalPoint> points);
}
