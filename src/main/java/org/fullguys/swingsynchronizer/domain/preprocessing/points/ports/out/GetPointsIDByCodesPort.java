package org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.out;

import org.javatuples.Pair;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface GetPointsIDByCodesPort {
    Map<String, Pair<Long, LocalDateTime>> getPointsIDByCodes(List<String> pointsCodes);
}
