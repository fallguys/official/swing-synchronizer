package org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.out;

import org.fullguys.swingsynchronizer.domain.entities.enums.WithdrawalPointState;

public interface UpdatePointsStatePort {
    void updatePointsState(WithdrawalPointState pointsState);
}
