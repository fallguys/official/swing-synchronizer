package org.fullguys.swingsynchronizer.domain.preprocessing.points.utils;

import java.util.Collection;
import java.util.List;

public class PointColumns {

    // COLUMN NAMES
    public static final String CODE                 = "CODIGO";
    public static final String NAME                 = "NOMBRE DE LA AGENCIA";
    public static final String UBIGEE               = "UBIGEO";
    public static final String DEPARTMENT           = "DEPARTAMENTO";
    public static final String PROVINCE             = "PROVINCIA";
    public static final String DISTRICT             = "DISTRITO";
    public static final String ADDRESS              = "DIRECCIÓN";
    public static final String HOUR_INIT_WEEKDAYS   = "HORA_INICIO_L_V";
    public static final String HOUR_END_WEEKDAYS    = "HORA_FIN_L_V";
    public static final String HOUR_INIT_SATURDAY   = "HORA_INICIO_SABADO";
    public static final String HOUR_END_SATURDAY    = "HORA_FIN_SABADO";

    public static final String CUSTOM_MESSAGE   = "MENSAJE DE ERROR";
    public static final String CUSTOM_UBIGEE_ID = "DISTRICT UBIGEE ID";

    // COLUMN INDEX
    public static final int INDEX_CODE                  = 0;
    public static final int INDEX_NAME                  = 1;
    public static final int INDEX_UBIGEE                = 2;
    public static final int INDEX_DEPARTMENT            = 3;
    public static final int INDEX_PROVINCE              = 4;
    public static final int INDEX_DISTRICT              = 5;
    public static final int INDEX_ADDRESS               = 6;
    public static final int INDEX_HOUR_INIT_WEEKDAYS    = 7;
    public static final int INDEX_HOUR_END_WEEKDAYS     = 8;
    public static final int INDEX_HOUR_INIT_SATURDAY    = 9;
    public static final int INDEX_HOUR_END_SATURDAY     = 10;

    public static final int INDEX_CUSTOM_MESSAGE    = 11;
    public static final int INDEX_CUSTOM_UBIGEE_ID  = 12;

    // COLUMN TYPES
    public static final Class<String> TYPE_CODE                 = String.class;
    public static final Class<String> TYPE_NAME                 = String.class;
    public static final Class<String> TYPE_UBIGEE               = String.class;
    public static final Class<String> TYPE_DEPARTMENT           = String.class;
    public static final Class<String> TYPE_PROVINCE             = String.class;
    public static final Class<String> TYPE_DISTRICT             = String.class;
    public static final Class<String> TYPE_ADDRESS              = String.class;
    public static final Class<String> TYPE_HOUR_INIT_WEEKDAYS   = String.class;
    public static final Class<String> TYPE_HOUR_END_WEEKDAYS    = String.class;
    public static final Class<String> TYPE_HOUR_INIT_SATURDAY   = String.class;
    public static final Class<String> TYPE_HOUR_END_SATURDAY    = String.class;

    public static final Class<String>  TYPE_CUSTOM_MESSAGE   = String.class;
    public static final Class<Long>    TYPE_CUSTOM_UBIGEE_ID = Long.class;

    // LIST OF ORIGINAL COLUMN NAMES
    public static final Collection<String> ORIGINAL_COLUMNS = List.of(
            CODE, NAME, UBIGEE, DEPARTMENT, PROVINCE, DISTRICT, ADDRESS, HOUR_INIT_WEEKDAYS, HOUR_END_WEEKDAYS, HOUR_INIT_SATURDAY, HOUR_END_SATURDAY
    );

    // LIST OF ORIGINAL COLUMN TYPES
    public static final Collection<Class<?>> COLUMNS_TYPES = List.of(
            TYPE_CODE, TYPE_NAME, TYPE_UBIGEE, TYPE_DEPARTMENT, TYPE_PROVINCE, TYPE_DISTRICT, TYPE_ADDRESS, TYPE_HOUR_INIT_WEEKDAYS, TYPE_HOUR_END_WEEKDAYS, TYPE_HOUR_INIT_SATURDAY, TYPE_HOUR_END_SATURDAY
    );

    // LIST OF COLUMN NAMES
    public static final Collection<Class<?>> ALL_COLUMNS_TYPES = List.of(
            TYPE_CODE, TYPE_NAME, TYPE_UBIGEE,
            TYPE_DEPARTMENT, TYPE_PROVINCE, TYPE_DISTRICT,
            TYPE_ADDRESS,
            TYPE_HOUR_INIT_WEEKDAYS, TYPE_HOUR_END_WEEKDAYS, TYPE_HOUR_INIT_SATURDAY, TYPE_HOUR_END_SATURDAY,
            TYPE_CUSTOM_MESSAGE, TYPE_CUSTOM_UBIGEE_ID
    );

    // LIST OF COLUMN TYPES
    public static final Collection<String> ALL_COLUMNS = List.of(
            CODE, NAME, UBIGEE,
            DEPARTMENT, PROVINCE, DISTRICT,
            ADDRESS,
            HOUR_INIT_WEEKDAYS, HOUR_END_WEEKDAYS, HOUR_INIT_SATURDAY, HOUR_END_SATURDAY,
            CUSTOM_MESSAGE, CUSTOM_UBIGEE_ID
    );
}
