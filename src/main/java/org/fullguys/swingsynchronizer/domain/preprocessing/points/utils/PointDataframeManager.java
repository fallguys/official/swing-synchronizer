package org.fullguys.swingsynchronizer.domain.preprocessing.points.utils;

import joinery.DataFrame;
import org.fullguys.swingsynchronizer.common.annotations.DataframeManager;
import org.fullguys.swingsynchronizer.common.constants.NumberConstants;
import org.fullguys.swingsynchronizer.common.constants.StringConstants;
import org.fullguys.swingsynchronizer.common.format.StringUtilsFormat;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.utils.UbigeeConstants;

import java.util.List;
import java.util.stream.IntStream;

@DataframeManager
public class PointDataframeManager {

    public DataFrame<Object> createExpandedColumnDataframe(DataFrame<Object> dataframe) {
        DataFrame.Function<List<Object>, Object> defaultMessage   = row -> PointConstants.DEFAULT_EMPTY_MESSAGE;
        DataFrame.Function<List<Object>, Object> defaultUbigeeID  = row -> PointConstants.DEFAULT_EMPTY_UBIGEE;

        var df = dataframe
                .add(PointColumns.CUSTOM_MESSAGE, defaultMessage)
                .add(PointColumns.CUSTOM_UBIGEE_ID, defaultUbigeeID);

        return fixUbigeesWithZerosForCasting(df);
    }

    public DataFrame<Object> fixUbigeesWithZerosForCasting(DataFrame<Object> dataframe) {
        IntStream.range(NumberConstants.ZERO, dataframe.length())
                .forEach(index -> {
                    var ubigeeCode = dataframe.row(index).get(PointColumns.INDEX_UBIGEE).toString();

                    var modified = StringUtilsFormat.fillWithChar(ubigeeCode,
                            StringConstants.CHAR_ZERO, UbigeeConstants.DISTRICT_UBIGEE_CODE_LENGTH);

                    dataframe.set(index, PointColumns.INDEX_UBIGEE, modified);
                });

        return dataframe;
    }

    public DataFrame<Object> generateEmptyBeneficiariesDataframe() {
        return new DataFrame<>(PointColumns.ALL_COLUMNS);
    }
}
