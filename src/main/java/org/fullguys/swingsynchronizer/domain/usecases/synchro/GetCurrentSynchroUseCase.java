package org.fullguys.swingsynchronizer.domain.usecases.synchro;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.entities.Synchro;
import org.fullguys.swingsynchronizer.domain.entities.enums.SynchroState;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.errors.SynchroNotFoundError;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.GetCurrentSynchroService;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out.GetSynchroByStatePort;

@UseCase
@RequiredArgsConstructor
public class GetCurrentSynchroUseCase implements GetCurrentSynchroService {

    private final GetSynchroByStatePort getSynchroByStatePort;

    @Override
    public Synchro getCurrentSynchro() {
        return getSynchroByStatePort.getSynchroByState(SynchroState.IN_PROCESS).orElseThrow(SynchroNotFoundError::new);
    }
}
