package org.fullguys.swingsynchronizer.domain.usecases.synchro;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.GetDataFileToProcessService;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out.GetDataFileToProcessPort;

import java.io.InputStream;

@UseCase
@RequiredArgsConstructor
public class GetDataFileToProcessUseCase implements GetDataFileToProcessService {

    private final GetDataFileToProcessPort getDataFileToProcessPort;

    @Override
    public InputStream getDataFileToProcess(String url){
        return getDataFileToProcessPort.getDataFileProcess(url);
    }
}
