package org.fullguys.swingsynchronizer.domain.usecases.synchro;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.entities.Ubigee;
import org.fullguys.swingsynchronizer.domain.entities.enums.UbigeeType;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.GetDistrictsUbigeesService;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out.GetUbigeeByTypePort;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetDistrictsUbigeesUseCase implements GetDistrictsUbigeesService {

    private final GetUbigeeByTypePort getUbigeeByTypePort;

    @Override
    public List<Ubigee> getDistrictsUbigees() {
        return getUbigeeByTypePort.getUbigeeByType(UbigeeType.DISTRICT);
    }
}
