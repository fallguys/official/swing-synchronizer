package org.fullguys.swingsynchronizer.domain.usecases.synchro;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.entities.Synchro;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.SaveSynchroService;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out.UpdateSynchroPort;

@UseCase
@RequiredArgsConstructor
public class SaveSynchroUseCase implements SaveSynchroService {

    private final UpdateSynchroPort updateSynchroPort;

    @Override
    public void saveSynchro(Synchro synchro) {
        updateSynchroPort.updateSynchro(synchro);
    }
}
