package org.fullguys.swingsynchronizer.domain.usecases.synchro;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.common.annotations.UseCase;
import org.fullguys.swingsynchronizer.domain.entities.Synchro;
import org.fullguys.swingsynchronizer.domain.preprocessing.beneficiaries.ports.in.PreprocessBeneficiariesService;
import org.fullguys.swingsynchronizer.domain.preprocessing.areas.ports.in.PreprocessContagionService;
import org.fullguys.swingsynchronizer.domain.preprocessing.points.ports.in.PreprocessPointsService;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.errors.UnknownSynchroFileTypeError;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.StartSynchroService;

import java.io.IOException;
import java.io.InputStream;

@UseCase
@RequiredArgsConstructor
public class StartSynchroUseCase implements StartSynchroService {

    private final PreprocessBeneficiariesService preprocessBeneficiariesService;
    private final PreprocessPointsService preprocessPointsService;
    private final PreprocessContagionService preprocessContagionService;

    @Override
    public Synchro startSynchro(InputStream datafile, Synchro synchro) throws IOException {
        switch (synchro.getFileType()) {
            case BENEFICIARY:
                return preprocessBeneficiariesService.preprocessBeneficiaries(datafile, synchro);
            case CONTAGION_AREA:
                return preprocessContagionService.preprocessContagion(datafile, synchro);
            case WITHDRAWAL_POINT:
                return preprocessPointsService.preprocessPoints(datafile, synchro);
            default:
                throw new UnknownSynchroFileTypeError(synchro.getFileType().name());
        }
    }
}
