package org.fullguys.swingsynchronizer.domain.usecases.synchro.errors;

public class SynchroNotFoundError extends RuntimeException {
    public SynchroNotFoundError() {
        super("No se ha encontrado ninguna sincronización en marcha para procesar");
    }
}
