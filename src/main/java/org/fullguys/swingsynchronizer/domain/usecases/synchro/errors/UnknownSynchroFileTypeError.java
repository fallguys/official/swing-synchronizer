package org.fullguys.swingsynchronizer.domain.usecases.synchro.errors;

public class UnknownSynchroFileTypeError extends RuntimeException {

    public UnknownSynchroFileTypeError(String fileType) {
        super(String.format("No se ha reconocido el tipo de archivo de datos  '%s' a sincronizar", fileType));
    }
}
