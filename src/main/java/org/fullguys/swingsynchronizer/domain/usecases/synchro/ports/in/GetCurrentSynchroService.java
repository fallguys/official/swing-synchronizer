package org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.Synchro;

public interface GetCurrentSynchroService {
    public Synchro getCurrentSynchro();
}
