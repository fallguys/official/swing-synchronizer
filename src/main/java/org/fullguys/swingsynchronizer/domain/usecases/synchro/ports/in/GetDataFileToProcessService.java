package org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public interface GetDataFileToProcessService {
    InputStream getDataFileToProcess(String url);
}
