package org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.Ubigee;

import java.util.List;

public interface GetDistrictsUbigeesService {
    List<Ubigee> getDistrictsUbigees();
}
