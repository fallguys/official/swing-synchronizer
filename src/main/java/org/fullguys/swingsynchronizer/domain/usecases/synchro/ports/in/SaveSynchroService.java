package org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.Synchro;

public interface SaveSynchroService {
    void saveSynchro(Synchro synchro);
}
