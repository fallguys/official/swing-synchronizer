package org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in;

import org.fullguys.swingsynchronizer.domain.entities.Synchro;

import java.io.IOException;
import java.io.InputStream;

public interface StartSynchroService {
    Synchro startSynchro(InputStream file, Synchro synchro) throws IOException;
}
