package org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public interface GetDataFileToProcessPort {
    InputStream getDataFileProcess(String url);
}
