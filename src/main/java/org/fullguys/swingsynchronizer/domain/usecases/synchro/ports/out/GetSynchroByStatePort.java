package org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out;

import org.fullguys.swingsynchronizer.domain.entities.Synchro;
import org.fullguys.swingsynchronizer.domain.entities.enums.SynchroState;

import java.util.Optional;

public interface GetSynchroByStatePort {
    Optional<Synchro> getSynchroByState(SynchroState synchroState);
}
