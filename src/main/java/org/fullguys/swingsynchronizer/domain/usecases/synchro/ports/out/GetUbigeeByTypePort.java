package org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out;

import org.fullguys.swingsynchronizer.domain.entities.Ubigee;
import org.fullguys.swingsynchronizer.domain.entities.enums.UbigeeType;

import java.util.List;

public interface GetUbigeeByTypePort {
    List<Ubigee> getUbigeeByType(UbigeeType ubigeeType);
}
