package org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.out;

import org.fullguys.swingsynchronizer.domain.entities.Synchro;

public interface UpdateSynchroPort {
    void updateSynchro(Synchro synchro);
}
