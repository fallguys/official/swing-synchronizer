package org.fullguys.swingsynchronizer.domain.usecases.synchro.utils;

public class UbigeeConstants {

    public static final int DISTRICT_UBIGEE_CODE_LENGTH = 6;
}
