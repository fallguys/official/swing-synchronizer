package org.fullguys.swingsynchronizer.jobs;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingsynchronizer.domain.entities.enums.SynchroState;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.GetCurrentSynchroService;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.GetDataFileToProcessService;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.SaveSynchroService;
import org.fullguys.swingsynchronizer.domain.usecases.synchro.ports.in.StartSynchroService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Configuration
@RequiredArgsConstructor
public class SynchronizerLauncher {

    private final Logger logger = LoggerFactory.getLogger(SynchronizerLauncher.class);

    private final GetCurrentSynchroService getCurrentSynchroService;
    private final StartSynchroService startSynchroService;
    private final GetDataFileToProcessService getDataFileToProcessService;
    private final SaveSynchroService saveSynchroService;

    @PostConstruct
    public void launchSynchro() {
        var synchro = getCurrentSynchroService.getCurrentSynchro();

        try {
             var datafile = getDataFileToProcessService.getDataFileToProcess(synchro.getDataFileURL());
            // var datafile = getClass().getClassLoader().getResourceAsStream("PuntosRetiro.csv");
            var modified = startSynchroService.startSynchro(datafile, synchro);
            modified.setState(SynchroState.COMPLETED);
            saveSynchroService.saveSynchro(modified);

        } catch (IOException e) {
            logger.error("Cannot read/use data file for synchronization", e);
            synchro.setState(SynchroState.INCOMPLETE);
            saveSynchroService.saveSynchro(synchro);

        } catch (Exception e) {
            logger.error("An exception ocurred during synchronization", e);
            synchro.setState(SynchroState.FAILED);
            saveSynchroService.saveSynchro(synchro);
        }
    }
}
